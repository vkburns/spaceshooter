﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour {
    public GameObject explosion;
    public GameObject playerExplosion;
    public float percentDrop = 25f;
    public GameObject Augment;
    
    public Transform Player;

    public int scoreValue;


    private GameController gameController;

    void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
    }

    void OnTriggerEnter(Collider other)
    {


        if (other.tag == "Boundary" || other.CompareTag("Enemy"))
        {
            return;
        }
        if (explosion != null)
        {
            var randChance = Random.Range(0f, 100f);
            if (randChance < percentDrop)
            {
                Instantiate(Augment, Player.position, Player.rotation);
            }
                Instantiate(explosion, transform.position, transform.rotation);
         }

        if (other.tag == "Player")
        {
            Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
            gameController.GameOver ();
        }
        gameController.AddScore(scoreValue);
        Destroy(other.gameObject);
        Destroy(gameObject);
    }
    

	
	// Update is called once per frame
	void Update () {
		
	}
}
